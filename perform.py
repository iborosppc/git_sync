import os

from Exscript.protocols.exception import TimeoutException
from Exscript.util.match import first_match
from Exscript.protocols import SSH2, Account
from paramiko import SSHClient, AutoAddPolicy
from paramiko.agent import AgentRequestHandler
from pexpect import pxssh
from config import HOST, PORT, PASS, USER, PROJECT_DIR, LOCAL_DIR
import sys


def ssh_connect(command):
    ssh_connection = pxssh.pxssh(timeout=200)
    ssh_connection.login(HOST, USER, PASS, port=PORT, sync_multiplier=5 )
    ssh_connection.sendline('cd '+PROJECT_DIR)
    ssh_connection.prompt()         # match the prompt
    ssh_connection.sendline("git "+command)
    ssh_connection.prompt()
    console_byte = ssh_connection.before
    ssh_connection.logout()
    console = console_byte.decode("utf-8").split("\r\n")
    return console


def ssh_2():
    # Connect
    client = SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(AutoAddPolicy())
    client.connect(HOST, PORT, username=USER, password=PASS)
    # Obtain session
    session = client.get_transport().open_session()
    # Forward local agent
    AgentRequestHandler(session)
    # Commands executed after this point will see the forwarded agent on
    # the remote end.
    # session.exec_command('cd '+PROJECT_DIR)
    session.exec_command('pwd')
    stdout = session.recv(1000)
    print(stdout)
    client.close()


def ssh_3(command):
    # account = read_login()  # Prompt the user for his name and password
    account = Account(USER, PASS)
    conn = SSH2()  # We choose to use SSH2
    conn.connect(hostname=HOST,port=PORT)  # Open the SSH connection
    conn.login(account)  # Authenticate on the remote host
    cdl = "cd "+ PROJECT_DIR
    git = "git "+ command
    pre_commands = ["uname -a", "pwd", cdl, "pwd", git]
    results = execute(conn, pre_commands)
    conn.send('exit\r')  # Send the "exit" command
    conn.close()  # Wait for the connection to close
    return results


def execute(conn, pre_commands):
    res_list = []
    for single in pre_commands:
        try:
            conn.execute(single)  # Execute the command
            rezult = repr(conn.response)
            print("The response was", rezult)
            res_list.append(rezult)
        except TimeoutException:
            os, hostname = first_match(conn, r'^(\S+)\s+(\S+)')
            print(os, hostname)
    return res_list


def local_git(command):
    #TODO  remove when development done
    os.chdir("..")
    os.chdir(LOCAL_DIR)
    console_newlines = os.popen("git " + command).readlines()
    console = list(map(str.strip,console_newlines))
    return console


def combined(command):
    ssh = ssh_3(command)
    # ssh = "text"
    local = local_git(command)
    ssh_branch = [i for i in ssh if i.startswith("On branch")]
    local_branch = [i for i in local if i.startswith("On branch")]
    if ssh_branch == [] or local_branch == []:
        print("SSH >>" + "".join(str(x) for x in ssh))
        print("LOCAL >>" + "".join(str(x) for x in local))
    else:
        print("SSH >>>" + ssh_branch)
        print("LOCAL >>>" + local_branch)


if __name__== "__main__":
        command = sys.argv[1]
        combined(command)
